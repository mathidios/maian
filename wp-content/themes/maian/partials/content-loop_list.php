<?php

$cat = get_post_type();




if($cat=="post"){
    ?>
    <style>
        
.post_thumbnail img {
    width: 100%;
    height: 300px;
}
.post_list_ul > li{
    width: 45%;
    margin: 0 2%;
    float: left;
    min-height: 412px;
}

.has-post-thumbnail:hover .laranja{
    background: #e7482cba;
    padding: 20px;
    margin-top: -300px;
    position: relative;
    margin-bottom: 20px;
    height: 300px;
    padding-top: 252px;
    -webkit-transition: width 2s; /* Safari */
    transition: width 2s;
}
.laranja{
    background: #e7482cba;
    padding: 20px;
    margin-top: -68px;
    position: relative;
    margin-bottom: 20px;
    -webkit-transition: width 2s; /* Safari */
    transition: width 2s;
}

        
    </style>
    <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="post_thumbnail"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( get_the_ID(), 'full' ); ?></a>
		</div>
	<?php } ?>
	<div class="laranja"><a href="<?php the_permalink(); ?>" style="
    text-decoration:  none;
    font-size: 20px;
    color:  #fff;
    font-weight:  bold;
"><?php the_title(); ?></a></div>
	<div><?php the_excerpt(); ?></div>
</li>
<?php    
}else{
?>


<style>

    h4:after{
        display: none !important;
    }
    .stm_post_info{
    width: 25% !important;
    border-right: 1px solid #ccc !important;
    padding: 2% !important;
    float: left !important;
    margin: 20px 0px 10px 0px !important;
    border-bottom: none !important;
    min-height: 295px;
    }
    .stm_post_info:hover{
background: #ececec;
    border: 1px solid #dad7d7 !important;
    border-radius: 5px;
    box-shadow: 0px 0px 6px 2px #c3c3c3;
    }
    .stripe_2, .post_excerpt, .post_read_more{
        text-align: center !important;
    }
    .limap{
        background: none !important;
    border: none !important;
    color: #000 !important;
    }
        @media only screen and (max-width: 991px){
.stm_post_info {
    width: 100% !important;
    border-right: 0px solid #ccc !important;
    padding: 27px !important;
    float: left !important;
    margin: 20px 0px 10px 0px !important;
    border-bottom: none !important;
    min-height: 252px;
    border: 1px solid #ccc !important;
    padding: 10px 0;
}
}
</style>

<li id="post-<?php the_ID(); ?>" <?php post_class( 'stm_post_info' ); ?>>
	<?php if( get_the_title() ): ?>
		<h4 class="stripe_2"><?php the_title(); ?></h4>
	<?php endif; ?>
<!--	<div class="stm_post_details clearfix">
		<ul class="clearfix">
			<li class="post_date">
				<i class="fa fa fa-clock-o"></i>
				<?php echo get_the_date(); ?>
			</li>
			<li class="post_by"><?php esc_html_e( 'Publicado por:', 'consulting' ); ?> <span><?php the_author(); ?></span></li>
			<li class="post_cat"><?php esc_html_e( 'Categoria:', 'consulting' ); ?>
				<span><?php echo implode( ', ', wp_get_post_categories( get_the_ID(), array( 'fields' => 'names' ) ) ) ?></span>
			</li>
		</ul>
		
	</div>-->
	<?php /*if ( has_post_thumbnail() ) { ?>
		<div class="post_thumbnail">
			<?php the_post_thumbnail( 'consulting-image-1110x550-croped' ); ?>
		</div>
	<?php }*/ ?>
	<div class="post_excerpt">
		<?php the_excerpt(); ?>
	</div>
	<div class="post_read_more">
		<a class="button bordered icon_right limap" href="<?php the_permalink(); ?>">
			<?php esc_html_e( 'leia mais', 'consulting' ); ?>
			
		</a>
	</div>
</li>
<?php
}
?>
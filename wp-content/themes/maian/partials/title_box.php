<?php
$post_id        = get_the_ID();
$is_shop        = false;
$is_product     = false;
$page_for_posts = get_option( 'page_for_posts' );
if ( is_home() || is_category() || is_search() || is_tag() || is_tax() ) {
    $post_id = $page_for_posts;
}
if ( ( function_exists( 'is_shop' ) && is_shop() )
   || ( function_exists( 'is_product_category' ) && is_product_category() )
   || ( function_exists( 'is_product_tag' ) && is_product_tag() )
   ) {
    $is_shop = true;
}
if ( function_exists( 'is_product' ) && is_product() ) {
    $is_product = true;
}
if ( $is_shop ) {
    $post_id = get_option( 'woocommerce_shop_page_id' );
}
$class = 'page_title';
if ( get_post_meta( $post_id, 'enable_transparent', true ) ) {
    $class .= ' transparent';
}
if ( get_post_meta( $post_id, 'disable_title', true ) ) {
    $class .= ' disable_title';
}


$categories = get_post_meta( $post_id, 'tipo');


@$cats = $_SERVER["REQUEST_URI"];
@$cats = explode("/stm_service_category/", $cats);

@$cat2 = explode('/?s=', @$cats[1]);



###############################################
###########   ALIMENTOS #######################
###############################################
//var_dump($categories[0]);
if($categories[0]=="alimentos" || @$cat2[0]=="food/" || @$cat2[0]=="food" || @$cats[1]=="food/" || @$cats[1]=="food" || @$cats[1]=="alimentos/" || @$cats[1]=="alimentos"){
    ?>
    <style>
        #main {
            background-color: #f2f2f2 !important;
        }
        .page_title.transparent {
    margin-bottom: 10px;
    display: none;
}
 .page_title {
    margin-bottom: 10px;
    display: none;
}
h1.page_title_2 {
    text-transform: none !important;
}
    </style>
    <img src="<?php echo site_url('/');?>/wp-content/uploads/2018/07/Banner-Produtos-Maian-Food-semfeixa.jpg" style="max-width: 100%;">
    
    <?php if($categories[0]=="alimentos" && is_single() ){ ?>
    <style>
        .entry-content{
            margin-top: 100px !important;
        }
        .page_title h1:after{
            display: none !important;
        }
        .page_title{
            height: 100px !important;
            margin-top: 100px !important;
        }
        .stm_post_info{
            display: none !important;
        }

    </style>

    <div class="" style="padding: 20px 0; background: #a67c52;">
      <div class="container">
        <div style="text-align:right;">
            <form action="<?php echo site_url('/');?>stm_service_category/food/" method="get">

                <input name="s" placeholder="Pesquisar" style="
                width: 20%;
                min-width: 243px;
                padding:  5px 10px;
                border-radius: 20px;
                border: 2px solid #ccc;
                color: #ccc !important;
                ">
            </form>
        </div>
    </div>
</div>



<?php }else{ ?>

<style>
    .page_title{
        display: none !important;
    }
</style>
    <?php
    $partners_obj = get_categories(array('taxonomy' => 'stm_service_category'));

foreach($partners_obj as $okb){
   
    if($okb->name=="Food"){
         //var_dump($okb->description);
        $desc = $okb->description;
    }
}
    ?>
<div class="container" style="padding: 10px 0;">
    <div style="text-align:right;">
        <form action="<?php echo site_url('/');?>stm_service_category/food/" method="get">

            <input name="s" placeholder="Pesquisar" style="
            width: 20%;
            min-width: 243px;
            padding:  5px 10px;
            border-radius: 20px;
            border: 2px solid #ccc;
            color: #ccc !important;
            ">
        </form>
    </div>
    <?php echo "<div class='desccat'>".$desc."</div>"; ?>
</div>
<?php
}
}
###############################################
###########   ALIMENTOS #######################
###############################################







###############################################
###########   HOME #######################
###############################################

if(@$cat2[0]=="home/" || @$cat2[0]=="home" || @$cats[1]=="home/" || @$cats[1]=="home" || @$cats[1]=="household/" || @$cats[1]=="household" || $categories[0]=="household"){
    ?>
    <style>
        #main {
            background-color: #f2f2f2 !important;
        }
        .page_title.transparent {
    margin-bottom: 10px;
    display: none;
}
    </style>
    <img src="<?php echo site_url('/');?>/wp-content/uploads/2018/07/Banner-Maian-Home-semfaixa.jpg" style="max-width: 100%;">
    
    <?php if($categories[0]=="household" && is_single() ){ ?>
    <style>
        .entry-content{
            margin-top: 100px !important;
        }
        .page_title h1:after{
            display: none !important;
        }
        .page_title{
            height: 100px !important;
            margin-top: 100px !important;
        }
        .stm_post_info{
            display: none !important;
        }

    </style>
    <div class="" style="padding: 20px 0; background: #7ec9d2;">
      <div class="container">
        <div style="text-align:right;">
            <form action="<?php echo site_url('/');?>stm_service_category/home/" method="get">

                <input name="s" placeholder="Pesquisar" style="
                width: 20%;
                min-width: 243px;
                padding:  5px 10px;
                border-radius: 20px;
                border: 2px solid #ccc;
                color: #ccc !important;
                ">
            </form>
        </div>
    </div>
</div>



<?php }else{ ?>

<style>
    .page_title{
        display: none !important;
    }
</style>
    <?php
    $partners_obj = get_categories(array('taxonomy' => 'stm_service_category'));

foreach($partners_obj as $okb){
   
    if($okb->name=="Home"){
         //var_dump($okb->description);
        $desc = $okb->description;
    }
}
    ?>
<div class="container" style="padding: 10px 0;">
    <div style="text-align:right;">
        <form action="<?php echo site_url('/');?>stm_service_category/home/" method="get">

            <input name="s" placeholder="Pesquisar" style="
            width: 20%;
            min-width: 243px;
            padding:  5px 10px;
            border-radius: 20px;
            border: 2px solid #ccc;
            color: #ccc !important;
            ">
        </form>
    </div>
    <?php echo "<div class='desccat'>".$desc."</div>"; ?>
</div>
<?php
}
}
###############################################
###########   HOME #######################
###############################################













###############################################
###########   BEAUTY #######################
###############################################

if(@$cat2[0]=="beauty/" || @$cat2[0]=="beauty" || @$cats[1]=="beauty/" || @$cats[1]=="beauty" || @$cats[1]=="cosmeticos/" || @$cats[1]=="cosmeticos" || $categories[0]=="cosmeticos"){
    ?>
    <style>
        #main {
            background-color: #f2f2f2 !important;
        }
        .page_title.transparent {
    margin-bottom: 10px;
    display: none;
}
    </style>
  
    <img src="<?php echo site_url('/');?>/wp-content/uploads/2018/07/Maian-Beautyv-semfaixa.jpg" style="max-width: 100%;">
  
    <?php if($categories[0]=="cosmeticos" && is_single() ){ ?>
    <style>
        .entry-content{
            margin-top: 100px !important;
        }
        .page_title h1:after{
            display: none !important;
        }
        .page_title{
            height: 100px !important;
            margin-top: 100px !important;
        }
        .stm_post_info{
            display: none !important;
        }

    </style>
    <div class="" style="padding: 20px 0; background: #b996bb;">
      <div class="container">
        <div style="text-align:right;">
            <form action="<?php echo site_url('/');?>stm_service_category/beauty/" method="get">

                <input name="s" placeholder="Pesquisar" style="
                width: 20%;
                min-width: 243px;
                padding:  5px 10px;
                border-radius: 20px;
                border: 2px solid #ccc;
                color: #ccc !important;
                ">
            </form>
        </div>
    </div>
</div>



<?php }else{ ?>

<style>
    .page_title{
        display: none !important;
    }
</style>
    <?php
    $partners_obj = get_categories(array('taxonomy' => 'stm_service_category'));

foreach($partners_obj as $okb){
   
    if($okb->name=="Beauty"){
         //var_dump($okb->description);
        $desc = $okb->description;
    }
}
    ?>
<div class="container" style="padding: 10px 0;">
    <div style="text-align:right;">
        <form action="<?php echo site_url('/');?>stm_service_category/beauty/" method="get">

            <input name="s" placeholder="Pesquisar" style="
            width: 20%;
            min-width: 243px;
            padding:  5px 10px;
            border-radius: 20px;
            border: 2px solid #ccc;
            color: #ccc !important;
            ">
        </form>
    </div>
    <?php echo "<div class='desccat'>".$desc."</div>"; ?>
</div>
<?php
}
}
###############################################
###########   BEAUTY #######################
###############################################
?>
<?php

$cat = get_post_type();
@$not = $_SERVER["REQUEST_URI"];
@$not2222 = $_SERVER["REQUEST_URI"];
@$not = explode("/noti", $not);

@$not2 = explode("category/v", $not2222);
//var_dump($not2);
@$not3 = explode("category/not", $not2222);
//var_dump($not3[1]);

                    if($cat=="post" && $not[1]=="cias/" 
                    || $cat=="post" && $not2[1]=="ideo/"  
                    || $cat=="post" && $not3[1]=="icia/"){}else{
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
                    
                $fullbanner = get_field('imagem_topo');

                    if(!empty($fullbanner)){
                    ?>

                        <img src="<?php echo $fullbanner; ?>" style="
    min-width: 100%;
    height: 300px;
">


                    <?php } }

if ( ! get_post_meta( $post_id, 'disable_title_box', true ) ): ?>
<div class="<?php echo esc_attr( $class ); ?>">
    <?php if ( ! get_post_meta( $post_id, 'disable_title', true ) || ! get_post_meta( $post_id, 'disable_breadcrumbs', true ) ): ?>
        <div class="container">

            <?php if ( ! get_post_meta( $post_id, 'disable_title', true ) ): ?>
                <?php if( consulting_page_title( false, esc_html__( 'News', 'consulting' ), esc_html__( 'Careers', 'consulting' ) ) ): ?>
                    <h1 class="h2"><?php echo consulting_page_title( false, esc_html__( 'News', 'consulting' ), esc_html__( 'Careers', 'consulting' ) ); ?></h1>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>
<?php endif; ?>
<?php
/*
Template Name: page orçamento
*/


 consulting_get_header(); ?>
 
 
 
 <style>
     .coluna1{
    float: left;
    margin-right: 20%;
}

.boto{
    background: #f26726 !important;
    color: #fff !important;
    font-weight: bold !important;
    font-size: 30px !important;
    padding: 10px 20px !important;
    border-radius: 5px !important;
    box-shadow: 3px 4px 6px 0px #888888 !important;
    text-decoration: none !important;
}
 </style>

	<div class="content-area" style="padding: 30px 0px;">
<div class="wpb_wrapper"><div class="vc_custom_heading text_align_center title_no_stripe" style="
    margin-top:  110px;
    margin-bottom: 50px;
"><h2 style="font-size: 45px;text-align: center">Solicite um orçamento</h2></div></div>

<form action="<?php echo site_url(); ?>/wp-admin/admin-ajax.php?action=uno" method="post" class="wpcf7-form" novalidate="novalidate">
    
    
    <div class="coluna1">
<p>
    <label> CNPJ*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="cnpj" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Inscrição estadual*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="inscEstadual" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Nome da empresa*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="nomeCliente" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Razão social*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="razaoSocial" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Inscrição Municipal*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="inscMunicipal" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Site*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="site" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Email*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> CPF do contato*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="cpf" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> RG do contato*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="rg" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Nome do contato*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="nomeContato" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
</div>





<div class="coluna2">

<p>
    <label> Observação*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="observacao" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> DDD*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="dddTelefone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Telefone*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="telefone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Endereço*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="endereco" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Numero*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="numero" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Bairro*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="bairro" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Cidade*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="cidade" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Sigla UF*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="siglaUf" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> CEP*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="cep" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>
<p>
    <label> Complemento*<br>
    <span class="wpcf7-form-control-wrap your-name">
        <input type="text" name="complementoEnd" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
    </span> 
    </label>
</p>











</div>
<div style="text-align:center;"><button class="boto" type="submit">Solicitar orçamento</button></div>
</form>

	</div>

<?php get_footer(); ?>
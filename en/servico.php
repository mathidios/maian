<?php
$id = $_GET['id'];
$result = mysqli_query($con,"SELECT * FROM db_servicos WHERE sis_controle=1 AND id=$id");
$row = mysqli_fetch_array($result);
$imagem = $row['imagem'];
?>

	<div id="slideshow">
		<?php
			$topo = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=3");
			$row_topo = mysqli_fetch_array($topo);
			$img_topo = $row_topo['imagem_topo'];
			echo "
			<div style='width:100%; height:200px; overflow:hidden;'>
				<img src='upload_arquivos/$img_topo' style='width:100%;'/>
			</div>
			";
		?>
	</div>
	<div class="wrapper" style="margin-bottom:60px;">
		<div class="tabname">
			SERVIÇOS
		</div>
		<div class="noticias_img" style="vertical-align:top;">
			<img  src=<?php echo "upload_arquivos/$imagem"; ?> />
		</div>
		<div style="display:inline-block; width:730px;">
			<h3 style="margin-top:90px; text-transform:uppercase;"><?php echo html_entity_decode(html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8")); ?></h3>
		</div>
		<div style="display:inline-block; width:960px;">
			<?php echo html_entity_decode(html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8")); ?>
		</div>
	</div>

<?php
$result = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=1");
$row = mysqli_fetch_array($result);
?>

	<div id="slideshow">
		<?php
			$topo = mysqli_query($con,"SELECT * FROM db_destaques WHERE sis_controle=1 AND imagem IS NOT NULL");
			echo "<div id='slides'><ul class='bjqs'>";
			while($row_topo = mysqli_fetch_array($topo)){
				$img_topo = $row_topo['imagem'];
				$url_topo = $row_topo['url'];
				$titulo_topo = $row_topo['titulo'];
				$target_topo = $row_topo['target'];
				echo "<li><a href='$url_topo' target='$target'><div style='width:100%; height:500px; background:url(upload_arquivos/$img_topo) center center no-repeat;'></div></a></li>";
				//echo "<li><a href='$url_topo' target='$target'><img src='upload_arquivos/$img_topo' style='width:100%;'/><!--<p class='bjqs-caption'>$titulo_topo</p>--></a></li>";
				//echo "<img src='upload_arquivos/$img_topo' style='width:100%;'/>";
			}
			echo "</ul></div>";
		?>
	</div>
	<div class="wrapper">
			<?php
			$result = mysqli_query($con,"SELECT * FROM db_noticias WHERE sis_controle=1 ORDER BY data DESC LIMIT 1");
			$i=0;
			while($row = mysqli_fetch_array($result)){
				$id = $row['id'];
				$imagem = $row['imagem'];
				$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
				$data = html_entity_decode(utf8_encode($row['sis_data']),NULL,"UTF-8");
				$data = date( 'd/m/y',strtotime($data));
				$texto = html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8");
				$texto = strip_tags(substr($texto,0,60)." ... ");
				$i++;
				if($i==1){
					echo "
					<a href='./noticia-$id'>
						<div class='noticias' style='margin-right:40px;'>
							<h3 style='margin:5px 25px; margin-top:20px;'>NOTÍCIAS <img src='img/noticias_icon.png' class='icon' /></h3>
							<hr>
							<div class='noticias_img'>
								<img src='upload_arquivos/$imagem' />
							</div>
							<div class='noticias_text'>
								<div style='height:121px;'>
									<h4 style='color:black;'>$titulo</h4>
									$texto
								</div>
								<a class='noticias_btn' href='./noticia-$id'>LEIA A MATÉRIA</a>
							</div>
						</div>	
					</a>";
				}
			}
			$result = mysqli_query($con,"SELECT * FROM db_novidades WHERE sis_controle=1 ORDER BY data DESC LIMIT 1");
			$i=0;
			while($row = mysqli_fetch_array($result)){
				$id = $row['id'];
				$imagem = $row['imagem'];
				$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
				$data = html_entity_decode(utf8_encode($row['sis_data']),NULL,"UTF-8");
				$data = date( 'd/m/y',strtotime($data));
				$texto = html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8");
				$texto = strip_tags(substr($texto,0,60)." ... ");
				$i++;
				if($i==1){
					echo "
					<a href='./novidade-$id'>
						<div class='noticias'>
							<h3 style='margin:5px 25px; margin-top:20px;'>NOVIDADES <img src='img/novidades_icon.png' class='icon' /></h3>
							<hr>
							<div class='noticias_img'>
								<img src='upload_arquivos/$imagem' />
							</div>
							<div class='noticias_text'>
								<div style='height:121px;'>
									<h4 style='color:black;'>$titulo</h4>
									$texto
								</div>
								<a class='noticias_btn'  href='./novidade-$id'>SAIBA MAIS</a>
							</div>
						</div>	
					</a>
					";
				}
			}
			?>	</div>





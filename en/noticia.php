<?php
$id = $_GET['id'];
$result = mysqli_query($con,"SELECT * FROM db_noticias WHERE sis_controle=1 AND id=$id");
$row = mysqli_fetch_array($result);
$imagem = $row['imagem'];
$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
$texto = html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8");
?>

	<div id="slideshow">
		<?php
			$topo = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=4");
			$row_topo = mysqli_fetch_array($topo);
			$img_topo = $row_topo['imagem_topo'];
			echo "
			<div style='width:100%; height:200px; overflow:hidden;'>
				<img src='upload_arquivos/$img_topo' style='width:100%;'/>
			</div>
			";
		?>
	</div>
	<div class="wrapper" style="margin-bottom:60px;">
		<div class="tabname">
			NOTÍCIAS
		</div>
		<div class="noticias_img" style="vertical-align:top;">
			<?php echo "<img src='upload_arquivos/$imagem' />"; ?>
		</div>
		<div style="display:inline-block; width:730px;">
			<h3 style="display:inline-block; width:500px;;"><?php echo $titulo; ?></h3><h5 style='float:right;display:inline-block;width:200px; text-align:right;'><a href='javascript:history.back();'>Voltar  &#10148;</a></h5>
			<hr style="width:99%;">
			<?php echo $texto; ?>
		</div>
		<?php 
		$fotos = mysqli_query($con,"SELECT arquivo FROM sis_imagens WHERE id_registro=$id");
		if(mysqli_num_rows($fotos)>0){ 
		?>
			<div style="margin:10px 15px; color:#F36523; text-align:center;">
				<hr style="position:relative; top:2px; background:#F36523; height:2px; margin-right:10px; width:400px; display:inline-block;"> FOTOS <hr style="position:relative; top:2px; background:#F36523; height:2px; margin-left:10px; width:400px; display:inline-block;">
				<div class="carousel" style="vertical-align:top;">
				<ul style='display:inline-block;'>
				<?php
				$fotos = mysqli_query($con,"SELECT arquivo FROM sis_imagens WHERE id_registro=$id");
				while($row = mysqli_fetch_array($fotos)) {
					$arquivo = $row['arquivo'];
					echo "<li><a href='upload_arquivos/$arquivo' data-lightbox='galeria'><img style='width:210px; height:180px; margin:10px;' src='upload_arquivos/$arquivo' /></a></li>";
				}
				?>
				</ul>
				</div>
				<a class='prev' style="position:relative; right:455px; top:-130px; font-size:50px; color:#F36523;">&#10096;</a>
				<a class='next' style="position:relative; left:455px; top:-130px; font-size:50px; color:#F36523;">&#10097;</a>
			</div>
		<?php } ?>

	</div>


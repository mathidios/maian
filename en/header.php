<?php 

//$host = "177.70.98.245";
$host = "localhost";
$db = "maian_app";
$user = "maian_user";
$pass = "m@1@n$01";
$con=mysqli_connect($host,$user,$pass,$db);
if(mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
//error_reporting(E_ALL); 
//ini_set('display_errors', 1);
?>
<!DOCTYPE HTML>
<html>
	<head>
	<title>Maian</title>
	<link rel="stylesheet" href="css/thickbox.css">
	<link rel="stylesheet" href="css/lightbox.css">
	<link rel="stylesheet" href="css/bjqs.css">
	<link rel="stylesheet" href="css/style.css">
	<meta charset="UTF-8">
</head>
	<div class="wrapper_menu">
		<div class="wrapper" style="height:60px; width:1100px;">
			<a  href="./" style="display:inline-block; width:220px; text-align:right; padding-top:15px; margin-right:10px;"><img src="img/logo.png" /></a>
			<div id="menu">
				<a href="./quemsomos" id="btn_empresa" onmouseover="hover_empresa_on();" onmouseout="hover_empresa_off();">EMPRESA <img id="img_empresa" style="position:relative; top:6px; margin-left:5px;" src="img/empresa_off.png" /></a>
				<div id="submenu_empresa" onmouseover="hover_empresa_on();" onmouseout="hover_empresa_off();" style="display:none; margin-left:64px; margin-top:2px; position:absolute; z-index:2000; padding:10px 2px; background:white; width:300px;height:50px;">
					<div style="width:49%; display:inline-block; vertical-align:top; text-align:left; border-right:1px solid #D1D3D4;">
						<a href="./quemsomos" style="color:#F36523;" ><span style="color:black;font-size:10px;position:relative; top:-3px;">&#10095;</span> QUEM SOMOS:</a></br>
						<span style="font-size:12px; color:black; margin-left:10px;">Missão,Visão,Valores</span>
					</div>
					<div style="width:43%; display:inline-block; vertical-align:top; text-align:left; padding-left:11px;">
						<a href="./mapa" style="color:#F36523;" ><span style="color:black;font-size:10px;position:relative; top:-3px;">&#10095;</span> LOCALIZAÇÃO:</a></br>
						<span style="font-size:12px; color:black; margin-left:10px;">Mapa,Rotas</span>
					</div>
				</div>
				<a href="./produtos" onmouseover="hover_on('produtos');" onmouseout="hover_off('produtos');">PRODUTOS <img id="img_produtos" style="position:relative; top:6px; margin-left:5px;" src="img/produtos_off.png" /></a>
				<a href="./servicos"  onmouseover="hover_on('servicos');" onmouseout="hover_off('servicos');" >SERVIÇOS <img id="img_servicos"  style="position:relative; top:6px; margin-left:5px;" src="img/servicos_off.png" /></a>
				<a href="./clientes"  onmouseover="hover_on('clientes');" onmouseout="hover_off('clientes');" id="btn_clientes">CLIENTES <img id="img_clientes"  style="position:relative; margin-left:5px;" src="img/clientes_off.png" /></a>
				<a href="./contato"  onmouseover="hover_on('contato');" onmouseout="hover_off('contato');" id="btn_contato">CONTATO <img id="img_contato" style="position:relative; top:3px; margin-left:5px;" src="img/contato_off.png" /></a>
				<img id="btn_busca" src="img/icon_lupa.png" onclick="show_busca()" style="cursor:pointer; position:relative; top:11px; width:39px;" />
				<form action="./busca" id="form_busca" method="POST" style="display:none;" >
					<input onmouseout="hide_busca()" style="display:inline-block; border:1px solid black; padding:9px; border-radius:5px; width:243px;"  type="text" class="input_busca" id="busca" name="busca" placeholder="Digite sua busca..." />
					<input onmouseover="show_busca()" style="display:inline-block; position:relative; left:-10px; top:13px; height:35px; border-radius: 5px;" type="image" src="img/icon_lupa_on.png" />
				</form>
			</div>
		</div>
	</div>


<?php
$result = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=1");
$row = mysqli_fetch_array($result);
?>

	<div id="slideshow">
		<?php
			$topo = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=4");
			$row_topo = mysqli_fetch_array($topo);
			$img_topo = $row_topo['imagem_topo'];
			echo "
			<div style='width:100%; height:200px; overflow:hidden;'>
				<img src='upload_arquivos/$img_topo' style='width:100%;'/>
			</div>
			";
		?>
	</div>
	<div class="wrapper">
		<div class="tabname">
			NOTÍCIAS
		</div>

			<?php
			$result = mysqli_query($con,"SELECT * FROM db_noticias WHERE sis_controle=1 ORDER BY data DESC");
			$i=0;
			while($row = mysqli_fetch_array($result)){
				$id = $row['id'];
				$imagem = $row['imagem'];
				$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
				$data = html_entity_decode(utf8_encode($row['sis_data']),NULL,"UTF-8");
				$data = date( 'd/m/y',strtotime($data));
				$texto = html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8");
				$texto = strip_tags($texto);
				$texto = substr($texto,0,110);
				$i++;
				if($i%2==0){
					echo "
					<a href='./noticia-$id'>
						<div class='noticias' style='margin-right:40px;'>
							<h3 style='margin:5px 25px; margin-top:20px;'>$titulo</h3>
							<hr>
							<div class='noticias_img'>
								<img src='upload_arquivos/$imagem' />
							</div>
							<div class='noticias_text'>
								<div style='height:121px;'>
									<h4 style='color:black;'>$titulo</h4>
									$texto
								</div>
								<a class='noticias_btn' href='./noticia-$id'>LEIA A MATÉRIA</a>
							</div>
						</div>	
					</a>
					";
				}else{
					echo "
					<a href='./noticia-$id'>
						<div class='noticias'>
							<h3 style='margin:5px 25px; margin-top:20px;'>$titulo</h3>
							<hr>
							<div class='noticias_img'>
								<img src='upload_arquivos/$imagem' />
							</div>
							<div class='noticias_text'>
								<div style='height:121px;'>
									<h4 style='color:black;'>$titulo</h4>
									$texto
								</div>
								<a class='noticias_btn' href='./noticia-$id'>LEIA A MATÉRIA</a>
							</div>
						</div>	
					</a>
					";
				}
			}
			?>
	</div>


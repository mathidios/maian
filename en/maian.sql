-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (i386)
--
-- Host: localhost    Database: maian_site
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `db_clientes`
--

DROP TABLE IF EXISTS `db_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_clientes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `nome_cliente` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `link` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_clientes`
--

LOCK TABLES `db_clientes` WRITE;
/*!40000 ALTER TABLE `db_clientes` DISABLE KEYS */;
INSERT INTO `db_clientes` VALUES (20,'br','2014-08-22 12:01:15',1,'m. dias branco','0005798001408723289.jpg',''),(19,'br','2014-08-22 12:00:57',1,'niely cosm&eacute;ticos','0208944001408723273.jpg',''),(17,'br','2014-08-22 11:59:04',1,'SNC/KANECHOM','0690866001408723218.gif',''),(18,'br','2014-08-22 12:00:32',1,'Skafe','0106769001408723251.jpg',''),(16,'br','2014-08-22 11:58:39',1,'Symrise','0093079001408723133.jpg',''),(15,'br','2014-08-22 11:58:03',1,'Vilma Alimentos','0433734001408723114.jpg',''),(11,'br','2014-08-11 14:28:10',0,'','',''),(21,'br','2014-08-22 12:01:38',1,'Givaudan','0677320001408723309.jpg',''),(22,'br','2014-08-22 12:01:52',1,'firmenich','0578120001408723324.jpg',''),(23,'br','2014-08-22 12:02:06',1,'cargill','0561358001408723336.jpg',''),(25,'br','2014-09-01 14:20:33',1,'Biocap','0460976001409597575.jpg',''),(26,'br','2014-09-19 11:00:28',0,'','',''),(27,'br','2014-09-22 13:39:35',0,'','','');
/*!40000 ALTER TABLE `db_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_destaques`
--

DROP TABLE IF EXISTS `db_destaques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_destaques` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `titulo` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `target` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_destaques`
--

LOCK TABLES `db_destaques` WRITE;
/*!40000 ALTER TABLE `db_destaques` DISABLE KEYS */;
INSERT INTO `db_destaques` VALUES (2,'br','2014-06-25 20:43:37',0,'','','',''),(3,'br','2014-06-25 20:45:01',1,'Home','http://maian.com.br/quemsomos','0200641001407508313.jpg','_self'),(8,'br','2014-07-14 11:51:39',1,'produtos','http://maian.com.br/produtos','0604580001407507639.jpg','_self'),(5,'br','2014-07-06 10:00:33',0,'','','',''),(6,'br','2014-07-06 10:28:57',0,'','','',''),(7,'br','2014-07-07 14:22:29',0,'','','',''),(9,'br','2014-08-08 11:19:39',1,'servicos','http://maian.com.br/servicos','0036291001408055081.jpg','_self'),(10,'br','2014-08-11 14:26:42',0,'','','',''),(11,'br','2014-09-10 14:31:56',0,'','','',''),(12,'br','2014-09-19 10:59:27',0,'','','',''),(13,'br','2014-09-22 13:37:35',0,'','','','');
/*!40000 ALTER TABLE `db_destaques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_newsletter`
--

DROP TABLE IF EXISTS `db_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_newsletter` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `email` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_newsletter`
--

LOCK TABLES `db_newsletter` WRITE;
/*!40000 ALTER TABLE `db_newsletter` DISABLE KEYS */;
INSERT INTO `db_newsletter` VALUES (1,'br','0000-00-00 00:00:00',1,'email@email.com.br'),(2,'br','0000-00-00 00:00:00',1,'teste@testec.com.br'),(5,'br','0000-00-00 00:00:00',1,'asd'),(4,'br','0000-00-00 00:00:00',1,'felippe.gallo2@gmail.com'),(6,'br','0000-00-00 00:00:00',1,'Newsletter cadastrada com sucesso'),(7,'br','0000-00-00 00:00:00',1,'felippe.gallo@gmail.com'),(8,'br','0000-00-00 00:00:00',1,'rafael.ramos@annova-ag.com.br');
/*!40000 ALTER TABLE `db_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_noticias`
--

DROP TABLE IF EXISTS `db_noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_noticias` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `data` date NOT NULL DEFAULT '0000-00-00',
  `titulo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `texto` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_noticias`
--

LOCK TABLES `db_noticias` WRITE;
/*!40000 ALTER TABLE `db_noticias` DISABLE KEYS */;
INSERT INTO `db_noticias` VALUES (22,'br','2014-07-29 12:39:11',1,'2014-07-28','Maian na FCE 2014','0788087001406648540.jpg','&lt;p&gt;Nosso ingrediente &eacute; a f&oacute;rmula do seu sucesso.&lt;/p&gt;&lt;p&gt;A MAIAN agradece sua participa&ccedil;&atilde;o.&lt;/p&gt;'),(19,'br','2014-07-15 08:53:01',0,'0000-00-00',NULL,'',NULL),(20,'br','2014-07-15 08:55:16',0,'0000-00-00',NULL,'',NULL),(21,'br','2014-07-25 14:49:06',0,'0000-00-00',NULL,'',NULL),(16,'br','2014-07-07 07:51:31',0,'0000-00-00',NULL,'',NULL),(5,'br','2014-02-17 17:11:07',0,'0000-00-00',NULL,'',NULL),(6,'br','2014-06-09 18:50:31',0,'0000-00-00',NULL,'',NULL),(7,'br','2014-06-09 18:59:52',0,'0000-00-00',NULL,'',NULL),(8,'br','2014-06-09 19:00:16',0,'0000-00-00',NULL,'',NULL),(9,'br','2014-06-09 19:00:27',0,'0000-00-00',NULL,'',NULL),(10,'br','2014-06-09 19:01:08',0,'0000-00-00',NULL,'',NULL),(13,'br','2014-06-25 08:16:42',0,'0000-00-00',NULL,'',NULL),(14,'br','2014-06-25 08:17:35',0,'0000-00-00',NULL,'',NULL),(15,'br','2014-06-25 13:30:14',0,'0000-00-00',NULL,'',NULL),(23,'br','2014-09-19 11:00:07',0,'0000-00-00',NULL,'',NULL);
/*!40000 ALTER TABLE `db_noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_novidades`
--

DROP TABLE IF EXISTS `db_novidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_novidades` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `data` date NOT NULL DEFAULT '0000-00-00',
  `titulo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `texto` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_novidades`
--

LOCK TABLES `db_novidades` WRITE;
/*!40000 ALTER TABLE `db_novidades` DISABLE KEYS */;
INSERT INTO `db_novidades` VALUES (1,'br','2014-07-15 08:55:38',1,'2014-07-28','&Oacute;leos Vegetais','0956992001406649396.jpg','&lt;p&gt;Gostar&iacute;amos de apresentar a nossos clientes nossa nova linha de &Oacute;leos Vegetais:&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Raspeberry;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Bluebarry;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Br&oacute;colis;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Damasco;&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Aveia;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Black Cumin;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Moringa;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Chilean Hazelnut;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Meadowfoam;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de Semente de Cranberry.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;Para mais informa&ccedil;&otilde;es entre em contato com os nossos atendentes: 1&lt;span style=&quot;line-height: 1.45em;&quot;&gt;1 4774.7010&lt;/span&gt;&lt;/p&gt;'),(2,'br','2014-09-11 09:44:36',1,'2014-09-11','Emoliente natural - Radia 7750','0036019001410443157.jpg','&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Eacute;ster 100 % Natural&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Tem excelente a&ccedil;&atilde;o emoliente e dispersante depigmentos, mantendo estabilidade na viscosidade com um percentual de at&eacute; 30% menor em compara&ccedil;&atilde;o com o Benzoato de Alquila e &Oacute;leo de R&iacute;cino.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Certificado EcoCert e Cosmos (produto 100% verde).&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Sensorial, toque seco, excelente espalhabilidade e r&aacute;pida absor&ccedil;&atilde;o na pele.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;APLICA&Ccedil;&Otilde;ES:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Removedor de maquiagem (l&iacute;quido e emuls&atilde;o).&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Len&ccedil;o umedecido para remo&ccedil;&atilde;o de maquiagem.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Condicionador capilar.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Reparador de pontas.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Protetor Solar (emuls&atilde;o ou spray).&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leo de banho e massagem.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;'),(3,'br','2014-09-22 13:39:13',0,'0000-00-00',NULL,'',NULL),(4,'br','2014-09-22 14:57:08',1,'2014-09-22','Radia 7887','0977480001411412501.jpg','&lt;p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Emulsificante &eacute;ster, l&iacute;quido viscoso, 100 % natural, para emuls&otilde;es W/O, em sistema a frio.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Caracter&iacute;stica n&atilde;o i&ocirc;nica.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Podendo-se adicionar at&eacute; 82% de &aacute;gua, obtendo-se assim, emuls&otilde;es W/O para formula&ccedil;&otilde;es com baixos custos.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fase oleosa aceit&aacute;vel 20%.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Emuls&atilde;o est&aacute;vel na faixa de PH de 2 a 7.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Certificado ECO CERT e COSMOS (produto 100% verde).&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Compat&iacute;vel com ess&ecirc;ncias.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;APLICA&Ccedil;&Otilde;ES&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme nutritivo.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme noturno.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Protetor solar.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme e lo&ccedil;&atilde;o para pele seca.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme protetivo infantil.&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme para massagem.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;'),(5,'br','2014-10-06 11:05:02',1,'2014-10-06','&Oacute;leo de Semente de Moringa','0532038001412608020.jpg','&lt;p&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;O &Oacute;leo de Semente de Moringa tamb&eacute;m &eacute; conhecido como &oacute;leo berr&ecirc;nico porque cont&eacute;m aproximadamente cerca de 7 a 10% de &aacute;cido berr&ecirc;nico, cuja subst&acirc;ncia &eacute; um grado muito utilizado em cosm&eacute;ticos, pois &eacute; um excepcional hidrantante para pele. Sua composi&ccedil;&atilde;o graxa &eacute; bastante semelhante ao &oacute;leo de oliva. Rico em vitaminas A e E, melhora a apar&ecirc;ncia e o brilho da pele.&lt;/span&gt;&lt;br&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Ajuda na preven&ccedil;&atilde;o do envelhecimento;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece hidrata&ccedil;&atilde;o, for&ccedil;a e brilho a pele;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Transmite brilho e hidrata os cabelos.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Indicado em formula&ccedil;&otilde;es como:&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Adi&ccedil;&atilde;o muito nutritiva para cremes faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme antirrugas;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Anto-aging, hidratante;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es corporais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Condicionadores;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;M&aacute;scaras capilares;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos de massagem;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos corporais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Sabonetes.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;div&gt;&lt;p&gt;&lt;b&gt;Indicado em formula&ccedil;&otilde;es como:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Adi&ccedil;&atilde;o muito nutritiva para cremes faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme antirrugas;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Anto-aging, hidratante;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es corporais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Condicionadores;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;M&aacute;scaras capilares;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos de massagem;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos corporais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Sabonetes.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/div&gt;&lt;/p&gt;Contate-nos para mais informa&ccedil;&otilde;es:&amp;nbsp;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;11 4774.7010&lt;/span&gt;&lt;/p&gt;'),(6,'br','2014-10-20 09:06:54',1,'2014-10-20','&Oacute;LEO DE SEMENTE DE CRANBERRY','0530159001413810536.jpg','&lt;p&gt;O &Oacute;leo de Semente de Cranberry possui um rico equil&iacute;brio de &aacute;cidos graxos, &Ocirc;mega 3, 6 e 9, al&eacute;m de graxos essenciais.&lt;/p&gt;&lt;p&gt;Vitamina A e E, fitoster&oacute;is, fosfolip&iacute;dios, compostos fen&oacute;licos e tamb&eacute;m pro-antocianinas. O produto melhora a elasticidade firmeza da pele.&lt;/p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Reduz a escama&ccedil;&atilde;o do couro cabeludo;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Mant&eacute;m a colora&ccedil;&atilde;o p&oacute;s-tintura;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Ajuda na preven&ccedil;&atilde;o do envelhecimento;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Auxilia no tratamento de peles ressecadas;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece hidrata&ccedil;&atilde;o, for&ccedil;a e brilho a pele;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Auxilia no tratamento de manchas;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece brilho e hidrata&ccedil;&atilde;o aos cabelos.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Indicado em formula&ccedil;&otilde;es como:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Anti-aging;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Scrubs;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Cremes faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;S&eacute;runs hidratantes;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Sabonetes;&amp;nbsp;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Produtos para prote&ccedil;&atilde;o solar;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Shampoos;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Condicionadores;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos corporais.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Contate-nos para mais informa&ccedil;&otilde;es:&amp;nbsp;11 4774.7010&lt;/p&gt;&lt;p&gt;&lt;/p&gt;'),(7,'br','2014-11-03 06:33:42',1,'2014-11-03','&Oacute;LEO DE SEMENTE DE BLUEBERRY','0868981001415014543.jpg','&lt;p&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;O &Oacute;leo de Semente de Blueberry &eacute; rico em &aacute;cidos graxos, &Ocirc;mega 3, &Ocirc;mega 6, compostos fen&oacute;licos (antioxidantes), al&eacute;m de antocianinas (respons&aacute;vel pela colora&ccedil;&atilde;o azulada do blueberry), um potente antioxidante muito conhecido em estudos. O &Oacute;leo de Semente de Blueberry melhora a elasticidade da pele.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Ajuda na preven&ccedil;&atilde;o do envelhecimento;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Auxilia no tratamento da Acne;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece hidrata&ccedil;&atilde;o, for&ccedil;a e brilho &agrave; pele;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Ativa a circula&ccedil;&atilde;o sangu&iacute;nea.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&Eacute; indicado em formula&ccedil;&otilde;es como anti-aging, cremes faciais, s&eacute;runs hidratastes, lo&ccedil;&otilde;es, gel para acne, cremes e gel de massagem, al&eacute;m de produtos para prote&ccedil;&atilde;o solar.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;'),(8,'br','2014-11-17 05:05:09',1,'2014-11-17','&Oacute;LEO DE SEMENTE DE RASPBERRY','0523787001416218902.jpg','&lt;p&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;O &Oacute;leo de Semente de Raspberry &eacute; uma fonte de &aacute;cidos graxos, &Ocirc;mega 3, &Ocirc;mega 6, compostos fen&oacute;licos (antioxidantes). al&eacute;m de antocianinas, um potente antioxidante muito conhecido em estudos para o tratamento de c&acirc;ncer, e polifen&oacute;is (&aacute;cido pel&aacute;gico), antioxidante que oferece prote&ccedil;&atilde;o contra raios UV e inibe a prolifera&ccedil;&atilde;o de melanc&oacute;licos - rico em vitamina E (tocoferol, tocotrienol e lutien), antioxidante de radicais livres.&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Este &oacute;leo melhora a elasticidade da pele.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Ajuda na preven&ccedil;&atilde;o do envelhecimento;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Auxilia no tratamento da Acne;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Colabora com o tratamento de psor&iacute;ase e eczema;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece hidrata&ccedil;&atilde;o, for&ccedil;a e brilho &agrave; pele.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&Eacute; indicado em formula&ccedil;&otilde;es como antiaging, cremes hidratastes, lo&ccedil;&otilde;es, gel para acne, gel de massagem e s&eacute;runs hidratantes faciais.&lt;/p&gt;&lt;/p&gt;'),(9,'br','2014-12-01 08:36:06',1,'2014-12-01','&Oacute;LEO DE SEMENTE DE BR&Oacute;COLIS','0063693001417441031.jpg','&lt;p&gt;&lt;p&gt;&Oacute;leo de Semente de Br&oacute;colis possui um rico equil&iacute;brio de &aacute;cidos grados, &Ocirc;mega 3, &amp;nbsp;&Ocirc;mega 6 e graxos essenciais, que proporcionam excelentes propriedades hidratastes.&amp;nbsp;&lt;/p&gt;&lt;p&gt;Produto n&atilde;o gorduroso e uma alternativa natural para o silicone. Em sua composi&ccedil;&atilde;o cont&eacute;m aproximadamente 50% de &aacute;cido er&uacute;cico (C22:1), 70% de &aacute;cido araquid&ocirc;nico e vitamina A.&amp;nbsp;&lt;/p&gt;&lt;p&gt;&Eacute; perfeito para hidratar e proteger os cabelos de tratamentos como progressiva e outros, ou peles secas e ressecadas.&lt;/p&gt;&lt;p&gt;&lt;b&gt;CARACTER&Iacute;STICAS&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Melhora a elasticidade e firmeza da pele;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Alternativa natural ao silicone;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Auxilia no tratamento de peles secas e ressecadas;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Fornece hidrata&ccedil;&atilde;o, for&ccedil;a e brilho aos cabelos.&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Indicado em formula&ccedil;&otilde;es como:&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Formula&ccedil;&otilde;es como shampoo;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Condicionador;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Cremes faciais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Hidratantes;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Lo&ccedil;&otilde;es;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Creme para pentear;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;M&aacute;scara capilar;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Oacute;leos corporais;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Produtos para massagem.&lt;/span&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;');
/*!40000 ALTER TABLE `db_novidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_paginas`
--

DROP TABLE IF EXISTS `db_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_paginas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem_topo` varchar(255) DEFAULT '',
  `imagem` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_paginas`
--

LOCK TABLES `db_paginas` WRITE;
/*!40000 ALTER TABLE `db_paginas` DISABLE KEYS */;
INSERT INTO `db_paginas` VALUES (1,'br','0000-00-00 00:00:00',1,'Quem Somos','&lt;p&gt;&lt;/p&gt;&lt;p&gt;A MAIAN Ltda surge com todo know-how da matriz Uruguaia Qu&iacute;mica S.A., um &iacute;cone em seu pa&iacute;s, com a tradi&ccedil;&atilde;o de mais de 40 anos de mercado.&lt;/p&gt;&lt;p&gt;Despontamos das necessidades de aproxima&ccedil;&atilde;o comercial entre empresas brasileiras e fornecedores estrangeiros. Atualmente, a corpora&ccedil;&atilde;o &eacute; mais que uma importadora e distribuidora de mat&eacute;rias-primas: desenvolvemos solu&ccedil;&otilde;es para ind&uacute;strias do segmento de alimentos, cosm&eacute;ticos e domissanit&aacute;rios.&lt;/p&gt;&lt;p&gt;Nosso maior objetivo &eacute; colaborar para o sucesso dos nossos parceiros, n&atilde;o apenas com produtos, mas sim, com solu&ccedil;&otilde;es personalizadas.&lt;/p&gt;&lt;p&gt;Trabalhamos em equipe com nossos clientes, oferecendo uma consultoria especializada, laborat&oacute;rio t&eacute;cnico, log&iacute;stica diferenciada e gerenciamento de estoque.&lt;/p&gt;&lt;p&gt;\r\n&lt;/p&gt;&lt;p&gt;&lt;b&gt;MISS&Atilde;O: &lt;/b&gt;Proporcionar\r\nconfian&ccedil;a e credibilidade na comercializa&ccedil;&atilde;o de mat&eacute;rias-primas dentro do\r\nmercado em que atuamos.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;u&gt;&lt;/u&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;b&gt;VIS&Atilde;O: &lt;/b&gt;Ser uma empresa reconhecida por agregar valor e inova&ccedil;&atilde;o, desenvolvendo\r\nconceitos que transmitam a evolu&ccedil;&atilde;o e a nobreza dos ingredientes.&lt;/p&gt;&lt;p&gt;&lt;b&gt;VALORES:&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;&Eacute;tica;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Compromisso;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Inova&ccedil;&atilde;o;&lt;/span&gt;&lt;br&gt;&lt;/li&gt;&lt;li&gt;&lt;span style=&quot;line-height: 1.45em;&quot;&gt;Comprometimento.&lt;/span&gt;&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;','0902391001407511861.jpg',''),(2,'br','0000-00-00 00:00:00',1,'Produtos','','0772195001405097872.jpg',''),(3,'br','0000-00-00 00:00:00',1,'Serviços','','0327781001405097886.jpg',''),(4,'br','0000-00-00 00:00:00',1,'Notícias','','0592350001405097900.jpg',''),(5,'br','0000-00-00 00:00:00',1,'Clientes','','0541914001405097913.jpg',''),(6,'br','0000-00-00 00:00:00',1,'Contato','','0797411001406842134.jpg',''),(7,'br','0000-00-00 00:00:00',1,'Localização','&lt;p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.45em; background-color: initial;&quot;&gt;AV. PORTUGAL, 263 - BAIRRO ITAQUI - ITAPEVI&lt;/span&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;CEP: 066960-060 - S&Atilde;O PAULO - BRASIL&lt;/p&gt;&lt;p&gt;FONE: +55 11 4774.7010&lt;/p&gt;&lt;br&gt;&lt;/p&gt;','0415488001406842066.jpg',''),(8,'br','0000-00-00 00:00:00',1,'Rodapé','&lt;p&gt;AV. PORTUGAL, 263 - BAIRRO ITAQUI - ITAPEVI&lt;br&gt;CEP: 066960-060 - S&Atilde;O PAULO - BRASIL&lt;br&gt;FONE: +55 11 4774.7010&lt;br&gt;&lt;/p&gt;','',''),(9,'br','0000-00-00 00:00:00',1,'Novidades','','0472458001407855242.jpg','');
/*!40000 ALTER TABLE `db_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_produtos`
--

DROP TABLE IF EXISTS `db_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_produtos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `id_categoria` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `nome_produto` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `ordem` int(255) DEFAULT '0',
  `url_amigavel` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_produtos`
--

LOCK TABLES `db_produtos` WRITE;
/*!40000 ALTER TABLE `db_produtos` DISABLE KEYS */;
INSERT INTO `db_produtos` VALUES (10,'br','2014-07-07 12:59:46',1,'1;2;5','&Aacute;cido C&iacute;trico',1,'acido-citrico'),(11,'br','2014-07-07 13:00:08',1,'1','&Aacute;cido S&oacute;rbico',1,'acido-sorbico'),(9,'br','2014-07-07 12:56:45',1,'1;2','&Aacute;cido Asc&oacute;rbico',1,'acido-ascorbico'),(68,'br','2014-07-25 17:11:45',1,'2','&Aacute;cido Gliox&iacute;lico',1,'acido-glioxilico'),(67,'br','2014-07-14 12:02:14',0,'','',0,''),(12,'br','2014-07-07 13:00:32',1,'1;2','Benzoato de S&oacute;dio (granulado)',1,'benzoato-de-sodio-granulado'),(13,'br','2014-07-07 13:01:02',1,'1;2','Bicarbonato de S&oacute;dio 0/13 - Extra fino',1,'bicarbonato-de-sodio-013-extra-fino'),(14,'br','2014-07-07 13:02:29',1,'1','Bicarbonato de S&oacute;dio 0/50 - Fino',1,'bicarbonato-de-sodio-050-fino'),(15,'br','2014-07-07 13:03:00',1,'1','Bicarbonato de Am&ocirc;nio',1,'bicarbonato-de-amonio'),(16,'br','2014-07-07 13:04:48',1,'1','Cacau Alcalino Black',1,'cacau-alcalino-black'),(17,'br','2014-07-07 13:05:20',1,'1','Cacau em P&oacute; alcalino',1,'cacau-em-po-alcalino'),(18,'br','2014-07-07 13:05:44',1,'1','Cacau em P&oacute; Lecitinado',1,'cacau-em-po-lecitinado'),(19,'br','2014-07-07 13:06:16',1,'1','Cacau em P&oacute; Natural',1,'cacau-em-po-natural'),(20,'br','2014-07-07 13:06:39',1,'1','Citrato de S&oacute;dio',1,'citrato-de-sodio'),(21,'br','2014-07-07 13:07:16',1,'1;2;5','Corante Amarelo Tartrazina',1,'corante-amarelo-tartrazina'),(22,'br','2014-07-07 13:07:47',1,'1;2;5','Corante Amarelo Crep&uacute;sculo',1,'corante-amarelo-crepusculo'),(24,'br','2014-07-07 13:09:02',1,'1;2;5','Corante Vermelho Ponceau',1,'corante-vermelho-ponceau'),(25,'br','2014-07-07 13:09:19',1,'1;2;5','Corante Amaranto',1,'corante-amaranto'),(26,'br','2014-07-07 13:09:54',1,'1;2;5','Corante Vermelho Allura',1,'corante-vermelho-allura'),(27,'br','2014-07-07 13:10:09',1,'1;2;5','Corante Azul Brilhante',1,'corante-azul-brilhante'),(28,'br','2014-07-07 13:10:42',1,'1','Etil - Vanilina',1,'etil-vanilina'),(30,'br','2014-07-07 13:11:56',1,'1','L - Leucina',1,'l-leucina'),(31,'br','2014-07-07 13:12:11',1,'1','L-Valina',1,'l-valina'),(32,'br','2014-07-07 13:12:30',1,'1','L - Isoleucina',1,'l-isoleucina'),(33,'br','2014-07-07 13:12:50',1,'1','Creatina',1,'creatina'),(34,'br','2014-07-07 13:14:07',1,'1','Fosfato Diss&oacute;dico Anidro',1,'fosfato-dissodico-anidro'),(35,'br','2014-07-07 13:14:37',1,'1','Fosfato Monoc&aacute;lcico',1,'fosfato-monocalcico'),(36,'br','2014-07-07 13:15:03',1,'1','Fosfato Monoss&oacute;dico Anidro',1,'fosfato-monossodico-anidro'),(37,'br','2014-07-07 13:15:35',1,'1','Fosfato Tric&aacute;lcico',1,'fosfato-tricalcico'),(38,'br','2014-07-07 13:15:52',1,'1','Glúten',1,'glten'),(39,'br','2014-07-07 13:16:06',1,'1;2;5','Goma Guar',1,'goma-guar'),(40,'br','2014-07-07 13:16:22',1,'1;5','Goma Xantana 80 e 200 Mesh',1,'goma-xantana-80-e-200-mesh'),(41,'br','2014-07-07 13:16:49',1,'1','Hexametafosfato de S&oacute;dio',1,'hexametafosfato-de-sodio'),(66,'br','2014-07-10 08:42:58',0,'','',0,''),(45,'br','2014-07-07 14:45:15',1,'1','Liquor de Cacau Natural',1,'liquor-de-cacau-natural'),(128,'br','2014-07-25 18:43:03',1,'1','Triglic&eacute;rides de Cadeia M&eacute;dia',1,'triglicerides-de-cadeia-media'),(47,'br','2014-07-07 14:46:58',1,'1','Manteiga de Cacau Natural',1,'manteiga-de-cacau-natural'),(48,'br','2014-07-07 14:47:21',1,'1','Menthol Cristal',1,'menthol-cristal'),(49,'br','2014-07-07 14:47:43',1,'1;2','Miristato de Isopropila - IPM',1,'miristato-de-isopropila-ipm'),(50,'br','2014-07-07 14:49:07',1,'1;2;5','&Oacute;leo Mineral USP',1,'oleo-mineral-usp'),(51,'br','2014-07-07 14:50:29',1,'1;2','Palmitato de Isopropila - IPP',1,'palmitato-de-isopropila-ipp'),(52,'br','2014-07-07 14:50:58',1,'1;2;5','Polisorbato 20',1,'polisorbato-20'),(53,'br','2014-07-07 14:52:04',1,'1;2;5','Polisorbato 60',1,'polisorbato-60'),(54,'br','2014-07-07 14:52:19',1,'1;2;5','Polisorbato 80',1,'polisorbato-80'),(55,'br','2014-07-07 14:52:50',1,'1','PGPR',1,'pgpr'),(56,'br','2014-07-07 14:53:16',1,'1','Pirofosfato de &Aacute;cido de S&oacute;dio',1,'pirofosfato-de-acido-de-sodio'),(57,'br','2014-07-07 14:54:30',1,'1','Pirofosfato Tetras&oacute;dico',1,'pirofosfato-tetrasodico'),(58,'br','2014-07-07 14:55:08',1,'1;2;5','Propilenoglicol USP',1,'propilenoglicol-usp'),(65,'br','2014-07-10 08:41:49',0,'','',0,''),(60,'br','2014-07-07 14:58:06',1,'1','Propionato de C&aacute;lcio',1,'propionato-de-calcio'),(61,'br','2014-07-07 14:58:27',1,'1;2','Sorbato de Pot&aacute;ssio',1,'sorbato-de-potassio'),(134,'br','2014-08-22 14:22:47',0,'','',0,''),(135,'br','2014-08-22 14:23:01',1,'5','Amida 60',1,'amida-60'),(64,'br','2014-07-07 15:00:20',1,'1','Vanilina',1,'vanilina'),(69,'br','2014-07-25 17:12:16',0,'','',0,''),(70,'br','2014-07-25 17:16:22',1,'2;5','&Aacute;cido Este&aacute;rico Dupla Press&atilde;o',1,'acido-estearico-dupla-pressao'),(71,'br','2014-07-25 17:17:20',1,'2;5','&Aacute;lcool Cet&iacute;lico',1,'alcool-cetilico'),(72,'br','2014-07-25 17:19:28',1,'2;5','&Aacute;lcool Cetoestear&iacute;lico',1,'alcool-cetoestearilico'),(73,'br','2014-07-25 17:20:08',1,'2','&Aacute;lcool Ceto Etoxilado 20 EO',1,'alcool-ceto-etoxilado-20-eo'),(74,'br','2014-07-25 17:20:36',1,'2','Amida 60 e 90',1,'amida-60-e-90'),(75,'br','2014-07-25 17:20:51',1,'2','Auto Mask - M&aacute;scara Capilar',1,'auto-mask-mascara-capilar'),(76,'br','2014-07-25 17:21:13',1,'2','Base Emulsificante W/O a Frio',1,'base-emulsificante-wo-a-frio'),(77,'br','2014-07-25 17:21:50',1,'2;5','Base Perolizante',1,'base-perolizante'),(78,'br','2014-07-25 17:22:32',0,'','',0,''),(79,'br','2014-07-25 17:22:32',1,'2;5','Beta&iacute;na de Coco (28-32%)',1,'betaina-de-coco-28-32'),(80,'br','2014-07-25 17:23:33',1,'2','C&acirc;nfora',1,'canfora'),(81,'br','2014-07-25 17:23:53',1,'2;5','Carb&ocirc;mero 940',1,'carbomero-940'),(82,'br','2014-07-25 17:24:15',1,'2','Carbonato de Guanidina',1,'carbonato-de-guanidina'),(83,'br','2014-07-25 17:24:44',0,'','',0,''),(84,'br','2014-07-25 17:29:06',1,'2','Corantes para Tintura Capilar',1,'corantes-para-tintura-capilar'),(85,'br','2014-07-25 17:29:59',1,'2','Emolientes Naturais',1,'emolientes-naturais'),(86,'br','2014-07-25 17:30:19',1,'2;5','Glicerina Bidestilada',1,'glicerina-bidestilada'),(87,'br','2014-07-25 17:31:08',1,'2','Goma Guar Quaternizada 14 S',1,'goma-guar-quaternizada-14-s'),(88,'br','2014-07-25 17:31:51',1,'2','Goma Xantana 80 e 200',1,'goma-xantana-80-e-200'),(89,'br','2014-07-25 17:32:08',0,'','',0,''),(90,'br','2014-07-25 17:32:16',1,'2;5','Lauril &Eacute;ter Sulfato de S&oacute;dio 27%',1,'lauril-eter-sulfato-de-sodio-27'),(91,'br','2014-07-25 17:32:49',1,'2;5','Lauril &Eacute;ter Sulfato de S&oacute;dio 70%',1,'lauril-eter-sulfato-de-sodio-70'),(92,'br','2014-07-25 17:33:17',1,'1;2','Lecitina de Soja',1,'lecitina-de-soja'),(93,'br','2014-07-25 17:33:36',1,'2','Mentol Cristal',1,'mentol-cristal'),(94,'br','2014-07-25 17:34:12',0,'','',0,''),(95,'br','2014-07-25 17:34:21',1,'2','&Oacute;leo de Mamona',1,'oleo-de-mamona'),(96,'br','2014-07-25 17:35:18',1,'2','&Oacute;leos Vegetais',1,'oleos-vegetais'),(97,'br','2014-07-25 17:36:00',1,'2','Palmitato de Octila',1,'palmitato-de-octila'),(99,'br','2014-07-25 17:37:38',1,'2','PVP K 30',1,'pvp-k-30'),(100,'br','2014-07-25 17:38:06',0,'','',0,''),(101,'br','2014-07-25 17:38:23',0,'','',0,''),(103,'br','2014-07-25 17:38:56',1,'2','PVP K 90',1,'pvp-k-90'),(105,'br','2014-07-25 17:39:47',1,'2;5','Quatern&aacute;rio de Am&ocirc;nio 50%',1,'quaternario-de-amonio-50'),(106,'br','2014-07-25 17:40:47',1,'2','Salicilato de Metila',1,'salicilato-de-metila'),(107,'br','2014-07-25 17:41:39',1,'2','Tioglicolato de Am&ocirc;nio 50%',1,'tioglicolato-de-amonio-50'),(109,'br','2014-07-25 17:42:46',1,'2','Triglic&eacute;rides de Ac. C&aacute;prico Capr&iacute;lico',1,'triglicerides-de-ac-caprico-caprilico'),(110,'br','2014-07-25 17:43:33',1,'1;5','Tripolifosfato de S&oacute;dio',1,'tripolifosfato-de-sodio'),(111,'br','2014-07-25 17:43:58',1,'2','Vaselina Pasta USP',1,'vaselina-pasta-usp'),(112,'br','2014-07-25 17:44:34',1,'2;5','&Aacute;cido Este&aacute;rico Tripla Press&atilde;o',1,'acido-estearico-tripla-pressao'),(113,'br','2014-07-25 17:45:10',0,'','',0,''),(114,'br','2014-07-25 17:46:06',1,'2;5','Betaina de Coco 34-36%',1,'betaina-de-coco-34-36'),(115,'br','2014-07-25 17:46:35',0,'','',0,''),(116,'br','2014-07-25 17:47:07',1,'2','Goma Guar Quaternizada 13 S',1,'goma-guar-quaternizada-13-s'),(117,'br','2014-07-25 17:48:32',1,'2;5','Carb&ocirc;mero 950',1,'carbomero-950'),(119,'br','2014-07-25 17:51:26',1,'2;5','Quatern&aacute;rio de Am&ocirc;nio 29%',1,'quaternario-de-amonio-29'),(122,'','2014-07-25 18:03:43',0,'','',0,''),(123,'br','2014-07-25 18:35:58',1,'5','Benzoato de S&oacute;dio',1,'benzoato-de-sodio'),(124,'br','2014-07-25 18:36:55',1,'5','Bicarbonato de S&oacute;dio',1,'bicarbonato-de-sodio'),(125,'br','2014-07-25 18:40:03',1,'5','&Aacute;cido Sulf&ocirc;nico 90%',1,'acido-sulfonico-90'),(126,'br','2014-07-25 18:40:32',1,'5','&Aacute;cido Sulf&ocirc;nico 96%',1,'acido-sulfonico-96'),(133,'br','2014-08-22 12:17:29',1,'5','Base Amaciante',1,'base-amaciante'),(129,'br','2014-07-25 18:43:34',0,'','',0,''),(132,'br','2014-08-06 09:29:18',1,'1','Benzoato de S&oacute;dio (P&oacute;)',1,'benzoato-de-sodio-po'),(131,'br','2014-07-25 18:44:31',0,'','',0,''),(136,'br','2014-09-22 13:38:33',0,'','',0,'');
/*!40000 ALTER TABLE `db_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_produtos_categorias`
--

DROP TABLE IF EXISTS `db_produtos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_produtos_categorias` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `titulo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem_topo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `texto` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_produtos_categorias`
--

LOCK TABLES `db_produtos_categorias` WRITE;
/*!40000 ALTER TABLE `db_produtos_categorias` DISABLE KEYS */;
INSERT INTO `db_produtos_categorias` VALUES (1,'br','2014-06-25 13:33:16',1,'Alimentos','0834424001405098855.jpg','0113869001407507525.jpg','&lt;p&gt;Nossos ingredientes para o segmento de alimentos incluem uma completa gama de produtos, com a finalidade de atender as ind&uacute;strias e necessidades deste amplo mercado. Oferecemos materiais de alta qualidade e tecnologia, visando o melhor atendimento aos nossos clientes e parceiros.&lt;/p&gt;'),(2,'br','2014-07-05 16:12:54',1,'Cosm&eacute;ticos','0441660001405098889.jpg','0757176001407507509.jpg','&lt;p&gt;Sempre comprometidos com a nossa meta de inova&ccedil;&atilde;o cont&iacute;nua, dispomos de uma linha completa de mat&eacute;rias-primas para o segmento de cosm&eacute;ticos.&lt;/p&gt;&lt;p&gt;Prestamos assessoria em aplica&ccedil;&otilde;es de produtos, cria&ccedil;&atilde;o e desenvolvimento de novos conceitos, para oferecer aos nossos clientes o que h&aacute; de melhor em tecnologia e materiais de primeira linha.&lt;br&gt;&lt;/p&gt;'),(3,'br','2014-07-06 10:00:37',0,NULL,NULL,'',NULL),(4,'br','2014-07-06 22:11:00',0,NULL,NULL,'',NULL),(5,'br','2014-07-14 12:01:39',1,'Household','','0688305001408057119.jpg','&lt;p&gt;Seguindo as tend&ecirc;ncias e necessidades desse mercado, focamos nosso crescimento atrav&eacute;s da inova&ccedil;&atilde;o das nossas mat&eacute;rias-primas, oferecendo produtos de alta performance, alinhados &agrave; qualidade e garantia de fornecimento. Estamos aptos a atender ao crescimento da demanda desse setor, cada vez mais promissor.&lt;/p&gt;'),(6,'br','2014-09-19 10:59:48',0,NULL,NULL,'',NULL),(7,'br','2014-09-22 13:38:13',0,NULL,NULL,'',NULL);
/*!40000 ALTER TABLE `db_produtos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `db_servicos`
--

DROP TABLE IF EXISTS `db_servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_servicos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sis_idioma` varchar(2) CHARACTER SET utf8 DEFAULT 'br',
  `sis_data` datetime DEFAULT '0000-00-00 00:00:00',
  `sis_controle` tinyint(1) DEFAULT '0',
  `titulo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem_topo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `texto` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_servicos`
--

LOCK TABLES `db_servicos` WRITE;
/*!40000 ALTER TABLE `db_servicos` DISABLE KEYS */;
INSERT INTO `db_servicos` VALUES (1,'br','2014-06-25 20:59:50',1,'Desenvolvimento de Produtos','0016515001403740810.jpg','0386091001408049287.jpg','&lt;p&gt;A Maian possui uma laborat&oacute;rio de pesquisa e desenvolvimento destinado a criar produtos diferenciados e inovadores, com o intuito de levar aos seus clientes todas as novidades, vindas de diversas regi&otilde;es do mundo.&lt;/p&gt;'),(2,'br','2014-07-07 12:20:59',1,'Know-How e Benef&iacute;cios','','0827324001408049595.jpg','&lt;p&gt;Com mais de 40 anos de experi&ecirc;ncia em com&eacute;rcio exterior, oferecemos uma variedade de mat&eacute;rias-primas de fornecedores internacionais, os quais s&atilde;o selecionados ap&oacute;s um rigoroso crit&eacute;rio de avalia&ccedil;&atilde;o e controle de qualidade, garantindo assim, o fornecimento cont&iacute;nuo de uma linha de produtos cada vez mais extensa e adequada a atender todas as necessidades dos nossos clientes. Gostamos de ajudar nossos parceiros a encontrar a mat&eacute;ria-prima adequada para as suas necessidades.&lt;br&gt;&lt;/p&gt;'),(3,'br','2014-07-07 12:32:15',1,'Log&iacute;stica','','0436082001408049910.jpg','&lt;p&gt;Nos dias de hoje, com o tempo cada vez mais limitado, a agilidade nas respostas tornou-se um item bastante valorizado. A Maian entende essa necessidade e, diante disso, transforma-se em um parceiro estrat&eacute;gico para os seus clientes.Pontualidade nas entregas, cumprimento de prazos e seguran&ccedil;a nos estoques s&atilde;o algumas das caracter&iacute;sticas que d&atilde;o valor adicional aos nossos servi&ccedil;os. O compromisso com essa tarefa faz com que nossos parceiros tenham uma tranquilidade maior em toda a sua carreira de fornecimento, fazendo com que eles possam se dedicar cada vez mais ao seu neg&oacute;cio.&lt;br&gt;&lt;/p&gt;'),(4,'br','2014-07-07 14:22:35',0,NULL,NULL,'',NULL),(5,'br','2014-09-10 14:33:21',0,NULL,NULL,'',NULL),(6,'br','2014-09-22 13:39:03',0,NULL,NULL,'',NULL);
/*!40000 ALTER TABLE `db_servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sis_imagens`
--

DROP TABLE IF EXISTS `sis_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sis_imagens` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `arquivo` varchar(255) DEFAULT '',
  `banco` varchar(255) DEFAULT '',
  `id_registro` varchar(255) DEFAULT NULL,
  `ordem` int(255) DEFAULT '0',
  `legenda` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sis_imagens`
--

LOCK TABLES `sis_imagens` WRITE;
/*!40000 ALTER TABLE `sis_imagens` DISABLE KEYS */;
INSERT INTO `sis_imagens` VALUES (11,'0812585001403740202.jpg','db_noticias','11',0,NULL),(12,'0901899001403740202.jpg','db_noticias','11',0,NULL),(13,'0355531001403740203.jpg','db_noticias','11',0,NULL),(14,'0504417001403740203.jpg','db_noticias','11',0,NULL),(15,'0606794001403740203.jpg','db_noticias','11',0,NULL),(16,'0795547001403740203.jpg','db_noticias','11',0,NULL),(17,'0814559001403740203.jpg','db_noticias','11',0,NULL),(18,'0036481001403740204.jpg','db_noticias','11',0,NULL),(19,'0050522001403740204.jpg','db_noticias','11',0,NULL),(20,'0006975001406648464.jpg','db_noticias','22',0,NULL),(21,'0378164001406648465.jpg','db_noticias','22',0,NULL),(23,'0271382001406648473.jpg','db_noticias','22',0,NULL),(25,'0516961001406648474.jpg','db_noticias','22',0,NULL),(26,'0046243001406648490.jpg','db_noticias','22',0,NULL),(27,'0082347001406648492.jpg','db_noticias','22',0,NULL),(30,'0176688001406648495.jpg','db_noticias','22',0,NULL),(31,'0858375001406648498.jpg','db_noticias','22',0,NULL),(34,'0477348001406648512.jpg','db_noticias','22',0,NULL),(38,'0558823001410478399.jpg','db_paginas','1',0,NULL),(39,'0150173001410478408.jpg','db_paginas','1',0,NULL),(40,'0695002001410478418.jpg','db_paginas','1',0,NULL),(41,'0638460001410478431.jpg','db_paginas','1',0,NULL),(42,'0808532001410478446.jpg','db_paginas','1',0,NULL);
/*!40000 ALTER TABLE `sis_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sis_imagens_textarea`
--

DROP TABLE IF EXISTS `sis_imagens_textarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sis_imagens_textarea` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) DEFAULT '',
  `arquivo` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sis_imagens_textarea`
--

LOCK TABLES `sis_imagens_textarea` WRITE;
/*!40000 ALTER TABLE `sis_imagens_textarea` DISABLE KEYS */;
/*!40000 ALTER TABLE `sis_imagens_textarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sis_login`
--

DROP TABLE IF EXISTS `sis_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sis_login` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `ultimo_acesso` datetime DEFAULT '0000-00-00 00:00:00',
  `login` varchar(255) DEFAULT '',
  `senha` varchar(255) DEFAULT '',
  `nome_usuario` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sis_login`
--

LOCK TABLES `sis_login` WRITE;
/*!40000 ALTER TABLE `sis_login` DISABLE KEYS */;
INSERT INTO `sis_login` VALUES (999,'2014-12-01 08:26:56','webmaster','MXFhejJ3c3g=','Administrador');
/*!40000 ALTER TABLE `sis_login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-02 20:53:27

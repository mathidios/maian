<?php
$categoria = $_GET['id'];
$result2 = mysqli_query($con,"SELECT * FROM db_produtos_categorias WHERE sis_controle=1 AND id=$categoria");
$row2 = mysqli_fetch_array($result2);
$imagem = $row2['imagem'];
$imagem_topo = $row2['imagem_topo'];
?>

	<div id="slideshow">
		<?php
			echo "
			<div style='width:100%; height:200px; overflow:hidden;'>
				<img src='upload_arquivos/$imagem_topo' style='width:100%;'/>
			</div>
			";
		?>
	</div>
	<div class="wrapper" style="margin-bottom:60px;">
		<div class="tabname">
			<?php echo html_entity_decode(html_entity_decode(utf8_encode($row2['titulo']),NULL,"UTF-8")); ?>
		</div>
		<div class="noticias_img" style="vertical-align:top;">
			<img  src=<?php echo "upload_arquivos/$imagem"; ?> />
		</div>
		<div style="display:inline-block; width:730px;">
			<h3><?php echo html_entity_decode(html_entity_decode(utf8_encode($row2['titulo']),NULL,"UTF-8")); ?></h3>
			<hr style="width:99%;">
			<?php echo html_entity_decode(html_entity_decode(utf8_encode($row2['texto']),NULL,"UTF-8")); ?>
		</div>
		<div style="margin:10px 15px;">
			<select class="input_select" onchange="produto_cat(this.value)" style="width:300px;">
				<option value="0">SEGMENTOS</option>
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_produtos_categorias WHERE sis_controle=1");
				while($row = mysqli_fetch_array($result)){
					$id = $row['id'];
					$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
					echo "
						<option value='$id'>$titulo</option>
					";
				}
				?>
			</select>
			<select class="input_select" onchange="produto(this.value)" style="width:500px; margin-left:100px;">
				<option value="0">PRODUTOS</option>
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_produtos WHERE sis_controle=1 ORDER BY url_amigavel ASC");
				while($row = mysqli_fetch_array($result)){
					$id = $row['id'];
					$cat = $row['id_categoria'];
					$titulo = html_entity_decode(utf8_encode($row['nome_produto']),NULL,"UTF-8");
					echo "
						<option value='$id'>$titulo</option>
					";
				}
				?>
			</select>
		</div>

		<div style="margin:10px 15px;">
			<a href="./produtos" class="noticias_btn" style="margin:0px; width:180px;">TODOS OS PRODUTOS</a>
		</div>
		<div style="margin:10px 15px;">
			<table class="produtos" style="width:100%;">
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_produtos WHERE sis_controle=1 AND id_categoria=$categoria OR id_categoria LIKE '$categoria;%' OR id_categoria LIKE '%;$categoria;%' OR id_categoria LIKE '%;$categoria' ORDER BY url_amigavel ASC");
				while($row = mysqli_fetch_array($result)){
					$id = $row['id'];
					$titulo = html_entity_decode(utf8_encode($row['nome_produto']),NULL,"UTF-8");
					$imagem = $row['imagem_topo'];
					$counter++;
					echo "
						<tr>
							<td style='width:30px; background:#eee; color:#F36523; text-align:center; border:1px solid #d2cbcb;'>$counter</td>
							<td style='width:675px;  background:#eee; color:black; padding:0px 10px; border:1px solid #d2cbcb;'><span style='color:#F36523; position:relative; top:-2px;'>▶ </span>$titulo</td>
							<td><a href='./contato-$id' class='noticias_btn' style='margin:0px; width:180px;'>SOLICITAR INFORMAÇÃO</a></td>
						</tr>
					";
				}
				?>
			</table>
		</div>

	</div>

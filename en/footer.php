 
	<div class="footer">
		<div class="wrapper">
			<div class="footer_square">
				<h3 style="color:black;">EMPRESA</h3>
				<a href="./quemsomos"><span style="color:black; font-size:10px;">&#10095;</span> Quem somos</a></br>
				<a href="./noticias"><span style="color:black; font-size:10px;">&#10095;</span> Notícias</a></br>
				<a href="./novidades"><span style="color:black; font-size:10px;">&#10095;</span> Novidades</a></br>
				<a href="./mapa"><span style="color:black; font-size:10px;">&#10095;</span> Localização</a></br>
			</div>
			<div class="footer_square">
				<h3 style="color:black;">SERVIÇOS</h3>
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_servicos WHERE sis_controle=1");
				while($row = mysqli_fetch_array($result)) {
					$id = $row['id'];
					$titulo = $row['titulo'];
					echo "<a href='./servico-$id'><span style='color:black; font-size:10px;'>&#10095;</span> $titulo</a></br>";
				}
				?>
			</div>
			<div class="footer_square">
				<h3 style="color:black;">PRODUTOS</h3>
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_produtos_categorias WHERE sis_controle=1");
				while($row = mysqli_fetch_array($result)) {
					$id = $row['id'];
					$titulo = $row['titulo'];
					echo "<a href='./produtos'><span style='color:black; font-size:10px;'>&#10095;</span> $titulo</a></br>";
				}
				?>
			</div>
			<div class="footer_square">
				<a href="./clientes"><h3 style="color:black;">CLIENTES</h3></a>
				<!--
				<div class="carousel_clientes">
				<ul>
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_clientes WHERE sis_controle=1 ORDER BY RAND()");
				while($row = mysqli_fetch_array($result)) {
					$link = $row['link'];
					$imagem = $row['imagem'];
					echo "<li><img style='width:135px; height:73px; margin:10px;' src='upload_arquivos/$imagem' /></li>";
				}

				?>
				</ul>
				</div>
				-->
			</div>
			<div class="footer_square" style="border-right:1px solid #d1d2d3;">
				<a href="./contato"><h3 style="color:black;">CONTATO</h3></a>
			</div>
		</div>
	</div>

	<div class="footer2">
		<div class="wrapper">
			<img src="img/site_iso_selo.png" width="100" height="98" style="float:right;" />
			<div style="display:inline-block; position:relative; top:-10px; margin-right:30px;">
				<img src="img/logo_rodape.gif" style="width:170px;"/>
			</div>
			<div style="display:inline-block; border-left:1px solid white; padding-left:30px;">
				<?php
				$result = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=8");
				$row = mysqli_fetch_array($result);
				echo  html_entity_decode(utf8_encode($row['texto']),NULL,"UTF-8");
				?>
			</div>
		</div>
	</div>

	<div class="footer" style="font-size:12px; padding:20px 0px;">
		<div class="wrapper">
			© 2014 Maiam Exportação e Importação. Todos os direitos reservados.<div style="float:right; display:inline;"><!--<a href="http://www.annova.ag" target="_blank">Desenvolvido por Annova-ag</a>--></div>
		</div>
	</div>
	<script src="js/jquery.js"></script>
	<script src="js/thickbox.js"></script>
	<script src="js/lightbox.min.js"></script>
	<script src="js/maskedinput.js"></script>
	<script src="js/carousel.js"></script>
	<script src="js/bjqs-1.3.js"></script>
	<script>
		jQuery(document).ready(function($) {
		    $('#slides').bjqs({
			'height' : 500,
			'width' : window.innerWidth,
			'responsive' : true,
			nexttext : '&#10093;', 
			prevtext : '&#10092;',
			showmarkers : false,
			usecaptions:false
		    });
		});
		
		 $(".carousel").jCarouselLite({
			btnNext: ".next",
			btnPrev: ".prev"
		});

		 $(".carousel_clientes").jCarouselLite({
			btnNext: ".next",
			btnPrev: ".prev"
		});
		
		jQuery(function($){
		   $("#telefone").mask("(99) 9999-9999");
		});

	</script>
	</body>
</html>
<?php mysqli_close($con); ?>

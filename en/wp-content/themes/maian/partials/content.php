<?php $vc_status = get_post_meta( get_the_ID() , '_wpb_vc_js_status', true);

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php if ( $vc_status != 'false' && $vc_status == true ): ?>
            <?php
            if ( is_singular('stm_event') ) {
                echo '<div class="event_content">';
                the_content();
                echo '</div>';
            } else {
                the_content();
            }
            ?>
            
        <?php else: ?>
            <?php if ( is_singular('stm_event') ) : ?>
                <?php
                $sidebar_type = get_theme_mod( 'event_sidebar_type', 'wp' );
                if ( $sidebar_type == 'wp' ) {
                    $sidebar_id = get_theme_mod( 'event_wp_sidebar', 'consulting-right-sidebar' );
                } else {
                    $sidebar_id = get_theme_mod( 'event_vc_sidebar' );
                }
                if ( ! empty( $_GET['sidebar_id'] ) ) {
                    $sidebar_id =  $_GET['sidebar_id'];
                }
                $structure = consulting_get_structure( $sidebar_id, $sidebar_type, get_theme_mod( 'blog_sidebar_position', 'right' ), get_theme_mod( 'blog_layout' ) ); ?>
                <?php echo $structure['content_before']; ?>
                <div class="without_vc">
                    <div class="event_content">
                        <?php get_template_part( 'partials/content', 'event-info' ); ?>
                        <?php the_content(); ?>
                        <?php get_template_part( 'partials/content', 'event-form' ); ?>
                    </div>
                </div>
                <?php echo $structure['content_after']; ?>
                <?php echo $structure['sidebar_before']; ?>
                <?php
                if ( $sidebar_id ) {
                    if ( $sidebar_type == 'wp' ) {
                        $sidebar = true;
                    } else {
                        $sidebar = get_post( $sidebar_id );
                    }
                }
                if ( isset( $sidebar ) ) {
                    if ( $sidebar_type == 'vc' ) { ?>
                    <style type="text/css" scoped>
                        <?php echo get_post_meta( $sidebar_id, '_wpb_shortcodes_custom_css', true ); ?>
                    </style>
                    <div class="sidebar-area stm_sidebar">
                        <?php echo apply_filters( 'the_content', $sidebar->post_content ); ?>
                    </div>
                    <?php } else { ?>
                    <div class="sidebar-area default_widgets">
                        <?php dynamic_sidebar( $sidebar_id ); ?>
                    </div>
                    <?php }
                }
                ?>
                <?php echo $structure['sidebar_after']; ?>
            <?php else: ?>
                <?php
                $sidebar_type = get_theme_mod( 'blog_sidebar_type', 'wp' );
                if ( $sidebar_type == 'wp' ) {
                    $sidebar_id = get_theme_mod( 'blog_wp_sidebar', 'consulting-right-sidebar' );
                } else {
                    $sidebar_id = get_theme_mod( 'blog_vc_sidebar' );
                }
                if ( ! empty( $_GET['sidebar_id'] ) ) {
                    $sidebar_id =  $_GET['sidebar_id'];
                }
                $structure = consulting_get_structure( $sidebar_id, $sidebar_type, get_theme_mod( 'blog_sidebar_position', 'right' ), get_theme_mod( 'blog_layout' ) ); ?>
                <?php echo $structure['content_before']; ?>
                <div class="without_vc">
<style>
    .aumentatitulo{
        text-transform: none !important;
    }
</style>
                    <?php 
                    
                    $cat = get_post_type();

                    if($cat=="post"){
                        
                    }else{
                    the_title( '<h1 class="h2 no_stripe page_title_2 aumentatitulo">', '</h1>' ); } ?>

                    <div class="post_details_wr">
                        <?php get_template_part( 'partials/content', 'post_details' ); ?>
                    </div>
                    
                    <?php
                    $cat = get_post_type();

                    if($cat!="post"){

                        ?>

                        <div class="wpb_text_column" style="
                        width: 75%;
                        float: left;
                        border-right: 1px solid #a67c52;
                        margin-right: 30px;
                        padding-right: 30px;
                        ">
                        
                        <?php
                    }else{
                        ?>
                        <style>
                            .page_title.transparent {
                                display: none !important;
                            }
                            .entry-content{
                                margin-top: 50px;
                            }
                        </style>
                        <style>
                            #main {
                                background: #f2f2f2;
                            }
                            .noticiaconteudo{
                                width: 80%;
                                float: left;
                            }
                            li{
                                list-style: none;
                            }
                            .titulocad{
                                text-transform: uppercase;
                                font-size: 20px;
                            }
                            .itemmenucat{
                                color: #000;
                                font-size: 20px;
                                border-left: 1px solid #777877;
                                padding-left: 20px;
                                padding-top: 10px;
                                padding-bottom: 10px;
                                margin-bottom: 10px;
                                text-decoration: none !important;
                            }
                            .stm_post_info{
                                display: none !important;
                            }
                            .dwdwdw{
    border-left: 3px solid #e7482c;
    padding-left: 17px;
}
                        </style>
                        <div class="content-area" style="padding: 30px 0px;">

                            <div class="noticiamenu">
                                <b class="titulocad">Categorias</b>
                                <div style="
                                margin-top: 20px;
                                ">
                                <?php
                                $categories = get_categories( array(
                                    'orderby' => 'name',
                                    'parent'  => 0
                                    ) );
                                
                                foreach ( $categories as $category ) {
                //var_dump('<pre>');
                $cats = get_the_category($post->ID);
                //var_dump($cats[0]->term_id);
                
                if($category->term_id == $cats[0]->term_id){
                    $coo = "dwdwdw";
                }else{
                    $coo = '';
                }
                if($category->term_id == 1){
                   echo '<a href="'.site_url().'/category/noticia/" class="itemmenucat '.$coo.'">Noticias</a><br />';
                }else{
                printf( '<a href="%1$s" class="itemmenucat '.$coo.'">%2$s</a><br />',
                    esc_url( get_category_link( $category->term_id ) ),
                    esc_html( $category->name )
                );
                }
            }
                                ?>
                            </div>
                        </div>
                        <div class="noticiaconteudo">
                            <div class="wpb_text_column" >
                                <?php
                            }
                            
                            ?>
                            <?php 
                            $cat = get_post_type();
                            if($cat=="post"){
                             the_title( '<h1 class="h2 no_stripe page_title_2">', '</h1>' );
                            }
                            the_content(); ?>
                        </div>
                    </div>
                    <?php
                    
                    $cat = get_post_type();

                    if($cat!="post"){
                        ?>

                        <div class="sidebar-area default_widgets">

                            <div class="primeirobo" style="
                            height: 90px;
                            ">
                            <a href="<?php site_url(); ?>/solicite-um-orcamento/" style="    background: #a67c52;
                            color: #fff;
                            padding: 20px;
                            border-radius: 17px;
                            font-size: 16px;
                            text-transform: uppercase;
                            text-decoration: none !important;
                            box-shadow: 2px 1px 6px 2px #c3c3c3;">Solicite um orçamento</a></div>
                            <div style="
                            height: 90px;
                            ">
                            <a href="<?php site_url(); ?>/fale-com-o-especialista/" style="    background: #eb5d13;
                            color: #fff;
                            padding: 20px;
                            border-radius: 17px;
                            font-size: 16px;
                            text-transform: uppercase;
                            text-decoration: none !important;
                            box-shadow: 2px 1px 6px 2px #c3c3c3;">Fale com o especialista</a></div>
                            <div style="
                            height: 90px;
                            ">
                            <!--<a href="#" style="    background: #005e20;
                            color: #fff;
                            padding: 20px 36px;
                            border-radius: 17px;
                            font-size: 16px;
                            text-transform: uppercase;
                            text-decoration: none !important;
                            box-shadow: 2px 1px 6px 2px #c3c3c3;">Entre em contato <i class="fa fa-whatsapp"></i></a>--></div>
                        </div>
                        <br/>
                        <br/>
                        <?php
                    }
                    ?>
                    <?php get_template_part( 'partials/content', 'post_bottom' ); ?>
                    <?php get_template_part( 'partials/content', 'about_author' ); ?>
                    <?php
                    wp_link_pages( array(
                        'before'      => '<div class="page-links"><label>' . esc_html__( 'Pages:', 'consulting' ) . '</label>',
                        'after'       => '</div>',
                        'link_before' => '<span>',
                        'link_after'  => '</span>',
                        'pagelink'    => '%',
                        'separator'   => '',
                        ) );
                        ?>
                    <?php /*if ( comments_open() || get_comments_number() ) : ?>
                        <div class="stm_post_comments">
                            <?php //comments_template(); ?>
                        </div>
                    <?php endif;*/ ?>
                </div>
                <?php echo $structure['content_after']; ?>
                <?php echo $structure['sidebar_before']; ?>
                <?php
                if ( $sidebar_id ) {
                    if ( $sidebar_type == 'wp' ) {
                        $sidebar = true;
                    } else {
                        $sidebar = get_post( $sidebar_id );
                    }
                }?>     

                <div class="sidebar-area default_widgets">
                    <?php dynamic_sidebar( 'left-sidebar' ); ?>
                </div>

                <?php echo $structure['sidebar_after']; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</article> <!-- #post-## -->
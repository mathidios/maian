<?php
$result = mysqli_query($con,"SELECT * FROM db_servicos WHERE sis_controle=1");
$row = mysqli_fetch_array($result);
?>

	<div id="slideshow">
		<?php
			$topo = mysqli_query($con,"SELECT * FROM db_paginas WHERE sis_controle=1 AND id=3");
			$row_topo = mysqli_fetch_array($topo);
			$img_topo = $row_topo['imagem_topo'];
			echo "
			<div style='width:100%; height:200px; overflow:hidden;'>
				<img src='upload_arquivos/$img_topo' style='width:100%;'/>
			</div>
			";
		?>
	</div>
	<div class="wrapper" style="margin-bottom:60px;">
		<div class="tabname">
			SERVIÇOS
		</div>
			<?php
			$result = mysqli_query($con,"SELECT * FROM db_servicos WHERE sis_controle=1");
			while($row = mysqli_fetch_array($result)){
				$id = $row['id'];
				$titulo = html_entity_decode(utf8_encode($row['titulo']),NULL,"UTF-8");
				$imagem = $row['imagem'];
				echo "
						<a href='./servico-$id'>
						<div style='display:inline-block; width:290px; text-align:center;'>
							<div class='noticias_img' style='vertical-align:top;'>
								<img src='upload_arquivos/$imagem' />
							</div>
							<h3 style='margin:5px 0px;'>$titulo</h3>
						</div>
						</a>
				";
			}
			?>
	</div>
